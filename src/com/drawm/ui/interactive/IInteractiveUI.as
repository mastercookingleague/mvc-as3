package com.drawm.ui.interactive{
import com.drawm.ui.*;
	public interface IInteractiveUI extends IUI{
		function out():void;
//        function hover():void;

        function down():void;
        function up():void;

        function get mouseX():Number;
		function get mouseY():Number;

		function get x():Number;
		function get y():Number;
		function set x(value:Number):void;
		function set y(value:Number):void;

		function get width():Number;
		function get height():Number;
		function set width(value:Number):void;
		function set height(value:Number):void;
	}
}