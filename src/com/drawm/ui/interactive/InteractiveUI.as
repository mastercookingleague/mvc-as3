package com.drawm.ui.interactive
{
import com.drawm.ui.*;

	public class InteractiveUI extends UI implements IInteractiveUI
	{
		public function InteractiveUI(id:String = null){
			this.mouseEnabled = true;
			super(id);
		}

	// Called by Controler ---------------------------------------------------------//
		public function up():void{}
		public function out():void{}
		public function down():void{}
	}
}