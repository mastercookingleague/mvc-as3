package com.drawm.ui.interactive {
import com.drawm.ui.*;

import flash.display.Bitmap;


	/**
	 * ...
	 * @author Damon Perron-Laurin
	 */
	public class InteractiveImage extends UIImage implements IInteractiveUI
	{
		
		public function InteractiveImage(id:String, texture:Bitmap = null){
			super(id, texture);
		}
		
		// Called by Controler ---------------------------------------------------------//
        public function up():void{}
        public function out():void{}
        public function down():void { }
	}
}