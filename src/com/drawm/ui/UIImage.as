package com.drawm.ui 
{
import flash.display.Bitmap;
import flash.display.Sprite;


	/**
	 * ...
	 * @author Damon Perron-Laurin
	 */
	public class UIImage extends Sprite implements IUI
	{

        private var _id:String;

        public function UIImage(id:String, texture:Bitmap){

            _id = id;
            UI.addUI(this);

            addChild(texture);
        }

        public function get id():String{
            return _id;
        }
	}
}