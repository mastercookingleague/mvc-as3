package com.drawm.mvc.controler
{

    import flash.events.KeyboardEvent;

	import com.drawm.mvc.Page;
	import com.drawm.mvc.event.MVCEvent;
	import com.drawm.mvc.model.Model;

    import flash.events.MouseEvent;

    import flash.geom.Point;

    import flash.events.Event;

	/**
	 * Le controler permet de capturer les intéraction de l'utilisateur (click, touch, drag, etc) 
	 * Quand l'utilisateur intéragie avec l'application, le controleur détermine 
	 * ce que l'application doit faire en retour.
	 * EX: Click sur un bouton -> change le bouton de couleur
	 * 
	 * Une fois que le retour de l'Application est déterminé, le controler appel le model pour qu'il modifie 
	 * les Data de l'application. 
	 * 
	 * <br/><br/>
	 * Il est inutile d'utiliser Controler seul, 
	 * il faut faire une subclass de Controler et override ses methods pour 
	 * qu'il puisse être completement fonctionnel.
	 * S'il est utilisé seul il est trop générique pour servir dans une application.
	 * <br/><br/>
	 * Methods à override :
	 * <br/><br/>
	 * <b>Lifecircle</b>
	 * <ul>
	 * 	<li>startControl():void</li>
	 * 	<li>stopControl():void</li>
	 * 	<li>start():void</li>
	 * 	<li>stop():void</li>
	 * 	<li>play():void</li>
	 * 	<li>resume():void</li>
	 * 	<li>pause():void</li>
	 * 	<li>dispose():void</li>
	 * </ul>
	 * <br/>
	 * <b>Intéraction</b>
	 * <ul>
     * 	<li>onTouchStationnary(MouseEvent):void </li>
	 * 	<li>onMouseOver(MouseEvent):void</li>
	 * 	<li>onTouchDown(MouseEvent):void</li>
	 * 	<li>onTouchOut(MouseEvent):void</li>
	 * 	<li>onTouchUp(MouseEvent):void</li>
	 * 	<li>onTouchMove(MouseEvent):void</li>
	 * 	<li>click(MouseEvent):void</li>
	 * </ul>
	 */
	public class Controller {

		protected var _model:Model;
		protected var _page:Page;

		/**
		 * Permet de faire le pont entre les intéractions de l'utilisateur et le comportement de l'application.
		 * Pour qu'un composant visuel soit persu comme intéractif, il doit absolument implementer IInteractiveUI
		 * 
		 * @param	model
		 * @param	page
		 */
		public function Controller(model:Model, page:Page){
			_model = model;
			_page  = page;
		}
		
		// Function lié au lifecircle de la page ---------------------------------------------------------- //

        final private function startControl():void{
            if(!_page.hasEventListener(MouseEvent.CLICK))
                _page.addEventListener(MouseEvent.CLICK, onClick);

            if(!_page.hasEventListener(MouseEvent.MOUSE_MOVE))
                _page.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);

            if(!_page.hasEventListener(MouseEvent.MOUSE_UP))
                _page.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

            if(!_page.hasEventListener(MouseEvent.MOUSE_DOWN))
                _page.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);

            if(!_page.hasEventListener(MouseEvent.MOUSE_OVER))
                _page.addEventListener(MouseEvent.MOUSE_OVER, hoverIn);

            if(!_page.hasEventListener(MouseEvent.MOUSE_OUT))
                _page.addEventListener(MouseEvent.MOUSE_OUT, hoverOut);

            if(!_page.hasEventListener(MVCEvent.LIST_CHANGE))
                _page.addEventListener(MVCEvent.LIST_CHANGE, onListChange);

            _page.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
            _page.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);

        }

        protected function onClick(event:MouseEvent):void{}
        protected function onListChange(event:Event):void{}

        protected function hoverIn(event:MouseEvent):void{}
        protected function hoverOut(event:MouseEvent):void{}

        protected function onMouseUp(event:MouseEvent):void{}
        protected function onMouseDown(event:MouseEvent):void{}
        protected function onMouseMove(touch:MouseEvent):void{}

        protected function onKeyDown(event:KeyboardEvent):void{}
        protected function onKeyUp(event:KeyboardEvent):void{}


        final public function stopControl():void{
            if(!_page.hasEventListener(MouseEvent.CLICK))
                _page.removeEventListener(MouseEvent.CLICK, onClick);

            if(!_page.hasEventListener(MouseEvent.MOUSE_UP))
                _page.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);

            if(!_page.hasEventListener(MouseEvent.MOUSE_DOWN))
                _page.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);

            if(!_page.hasEventListener(MouseEvent.MOUSE_OVER))
                _page.removeEventListener(MouseEvent.MOUSE_OVER, hoverIn);

            if(!_page.hasEventListener(MouseEvent.MOUSE_OUT))
                _page.removeEventListener(MouseEvent.MOUSE_OUT, hoverOut);

            if(!_page.hasEventListener(MVCEvent.LIST_CHANGE))
                _page.removeEventListener(MVCEvent.LIST_CHANGE, onListChange);

            _page.stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
            _page.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        }

        /**
         * Appel le model pour rendre la page visible
         */
        public function start():void{
            _model.start();
        }

        /**
         * Active l'intéraction de la page avec startControl() et
         * appel le model pour qu'il amorce le fonctionnement de la page
         */

        public function resume():void {
            startControl();
            _model.resume();
        }
        /**
         * Dit au model d'arrêter temporairement la page.
         * Arrête toute intéraction avec la page, mais elle est encore visible.
         */
        public function pause():void{
            _model.pause();
            stopControl();
        }
        /**
         * Dit au model de faire disparaitre la page de la displaylist
         */
        public function stop():void{
            _model.stop();
        }
        /**
         * Vide la mémoire du controleur
         * Dit au model de vider sa mémoire
         */
        //La page disparais de la memoire
        public function dispose():void {
            _model.dispose();
            _model = null;
            _page = null;
        }

        public function update():void {
            _model.update();
        }

		public function changePage(page:Page):void {
			_page = page;
		}
	}
}